package org.example.dghoughihafsa.Repository;

import org.example.dghoughihafsa.Entity.Adjoint;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdjointRepository extends JpaRepository<Adjoint,Long> {
}
