package org.example.dghoughihafsa;

import org.example.dghoughihafsa.Controller.EmployerController;
import org.example.dghoughihafsa.Entity.Employer;
import org.example.dghoughihafsa.Entity.SuperClasses.Person;
import org.example.dghoughihafsa.Services.EmployerService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DghoughihafsaApplication {

    public static void main(String[] args) {

        SpringApplication.run(DghoughihafsaApplication.class, args);
    }
/*
    @Bean
    public CommandLineRunner commandLineRunner(EmployerController employerController) {
        return (args) -> {

            Person employeCree = employerController.creerEmp("John Doe");
            System.out.println("Employé créé:");
            employerController.afficherEmp(employeCree.getId());


            Person employeModifie = employerController.modifierEmp(employeCree.getId(), "Jane Doe");
            System.out.println("Employé modifié:");
            employerController.afficherEmp(employeModifie.getId());


            employerController.supprimerEmp(employeModifie.getId());
            System.out.println("Employé supprimé.");


            employerController.afficherEmp(employeModifie.getId());
        };
    }

 */
    
}
