package org.example.dghoughihafsa.Controller;

import org.example.dghoughihafsa.Entity.SuperClasses.Person;
import org.example.dghoughihafsa.Services.EmployerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/employer")
public class EmployerController {
    private EmployerService employerService;


    @Autowired

    public EmployerController(EmployerService employerService) {
        this.employerService = employerService;
    }

    @GetMapping("/afficher/{id}")
    public void afficherEmp(@PathVariable Long id) {
        employerService.afficherEmp(id);
    }

    @PostMapping("/creer")
    public Person creerEmp(@RequestBody String nom) {
        return employerService.creerEmp(nom);
    }

    @PutMapping("/modifier/{id}")
    public Person modifierEmp(@PathVariable Long id, @RequestParam String nouveauNom) {
        return employerService.modifierEmp(id, nouveauNom);
    }



    @DeleteMapping("/supprimer/{id}")
    public void supprimerEmp(@PathVariable Long id) {
        employerService.supprimerEmp(id);
    }
}
