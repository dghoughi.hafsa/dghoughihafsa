package org.example.dghoughihafsa.Controller;

import org.example.dghoughihafsa.Entity.Associe;
import org.example.dghoughihafsa.Services.AssocieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/associe")
public class AssocieController {
    private AssocieService associeService;
    @Autowired
    public AssocieController(AssocieService associeService) {
        this.associeService = associeService;
    }
    @GetMapping("/afficher/{id}")
    public void afficherassocie(@PathVariable Long id) {
        associeService.afficherAss(id);
    }

    @PostMapping("/creer")
    public Associe creerassocie(@RequestBody String nom) {
        return associeService.creerAss(nom);
    }

    @PutMapping("/modifier/{id}")
    public Associe modifierassocie(@PathVariable Long id, @RequestParam String nouveauNom) {
        return associeService.modifierAss(id, nouveauNom);
    }

    @DeleteMapping("/supprimer/{id}")
    public void supprimerassocie(@PathVariable Long id) {
        associeService.supprimerAss(id);
    }
}
