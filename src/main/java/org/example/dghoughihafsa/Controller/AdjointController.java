package org.example.dghoughihafsa.Controller;

import org.example.dghoughihafsa.Entity.Adjoint;
import org.example.dghoughihafsa.Services.AdjointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("adjoint")
public class AdjointController {
    private AdjointService adjointService;

    @Autowired
    public AdjointController(AdjointService adjointService) {
        this.adjointService = adjointService;
    }



    @GetMapping("/afficher/{id}")
    public void afficherAdjoint(@PathVariable Long id) {
        adjointService.afficherAd(id);
    }

    @PostMapping("/creer")
    public Adjoint creerAdjoint(@RequestBody String nom) {
        return adjointService.creerAd(nom);
    }

    @PutMapping("/modifier/{id}")
    public Adjoint modifierAdjoint(@PathVariable Long id, @RequestParam String nouveauNom) {
        return adjointService.modifierAd(id, nouveauNom);
    }

    @DeleteMapping("/supprimer/{id}")
    public void supprimerAdjoint(@PathVariable Long id) {
        adjointService.supprimerAd(id);
    }
}
