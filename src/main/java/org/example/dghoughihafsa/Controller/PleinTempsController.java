package org.example.dghoughihafsa.Controller;

import org.example.dghoughihafsa.Entity.SuperClasses.Person;
import org.example.dghoughihafsa.Services.PleinTempsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/pleintemps")
public class PleinTempsController {
    private PleinTempsService pleinTempsService;
    @Autowired

    public PleinTempsController(PleinTempsService pleinTempsService) {
        this.pleinTempsService = pleinTempsService;
    }
    @GetMapping("/afficher/{id}")
    public void afficherPleinTemps(@PathVariable Long id) {
        pleinTempsService.afficherPleinTemps(id);
    }

    @PostMapping("/creer")
    public Person creerPleinTemps(@RequestBody String nom) {
        return pleinTempsService.creerPleinTemps(nom);
    }

    @PutMapping("/modifier/{id}")
    public Person modifierPleinTemps(@PathVariable Long id, @RequestParam String nouveauNom) {
        return pleinTempsService.modifierPleinTemps(id, nouveauNom);
    }

    @DeleteMapping("/supprimer/{id}")
    public void supprimerPleinTemps(@PathVariable Long id) {
        pleinTempsService.supprimerPleinTemps(id);
    }
}
