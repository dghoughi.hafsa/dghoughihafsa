package org.example.dghoughihafsa.Services;

import org.example.dghoughihafsa.Entity.Associe;
import org.example.dghoughihafsa.Repository.AssocieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AssocieService {
    private AssocieRepository associeRepository;
    @Autowired

    public AssocieService(AssocieRepository associeRepository) {
        this.associeRepository = associeRepository;
    }
    public Associe creerAss(String nom) {
        Associe Associe = new Associe();
        Associe.setNom(nom);
        return associeRepository.save(Associe);
    }

    public Associe modifierAss(Long id, String nouveauNom) {
        Associe Associe = associeRepository.findById(id).orElse(null);
        if (Associe != null) {
            Associe.setNom(nouveauNom);
            return associeRepository.save(Associe);
        }
        return null;
    }

    public void afficherAss(Long id) {
        Associe Associe = associeRepository.findById(id).orElse(null);
        if (Associe != null) {
            System.out.println("ID: " + Associe.getId() + ", Nom: " + Associe.getNom());
        } else {
            System.out.println("Personne non trouvée pour l'ID: " + id);
        }
    }

    public void supprimerAss(Long id) {
        associeRepository.deleteById(id);
    }
}
