package org.example.dghoughihafsa.Services;

import org.example.dghoughihafsa.Entity.Employer;
import org.example.dghoughihafsa.Repository.EmployerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployerService {
    private EmployerRepository employerRepository;
    @Autowired

    public EmployerService(EmployerRepository employerRepository) {
        this.employerRepository = employerRepository;
    }

    public Employer creerEmp(String nom) {
        Employer employer = new Employer();
        employer.setNom(nom);
        return employerRepository.save(employer);
    }

    public Employer modifierEmp(Long id, String nouveauNom) {
        Employer employer = employerRepository.findById(id).orElse(null);
        if (employer != null) {
            employer.setNom(nouveauNom);
            return employerRepository.save(employer);
        }
        return null;
    }

    public void afficherEmp(Long id) {
        Employer employer = employerRepository.findById(id).orElse(null);
        if (employer != null) {
            System.out.println("ID: " + employer.getId() + ", Nom: " + employer.getNom());
        } else {
            System.out.println("Personne non trouvée pour l'ID: " + id);
        }
    }

    public void supprimerEmp(Long id) {
        employerRepository.deleteById(id);
    }
}
