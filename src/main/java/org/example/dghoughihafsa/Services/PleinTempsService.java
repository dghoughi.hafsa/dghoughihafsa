package org.example.dghoughihafsa.Services;

import org.example.dghoughihafsa.Entity.PleinTemps;
import org.example.dghoughihafsa.Repository.PleinTempsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PleinTempsService {
    private PleinTempsRepository pleinTempsRepository;
    @Autowired

    public PleinTempsService(PleinTempsRepository pleinTempsRepository) {
        this.pleinTempsRepository = pleinTempsRepository;
    }
    public PleinTemps creerPleinTemps(String nom) {
        PleinTemps pleinTemps = new PleinTemps();
        pleinTemps.setNom(nom);
        return pleinTempsRepository.save(pleinTemps);
    }

    public PleinTemps modifierPleinTemps(Long id, String nouveauNom) {
        PleinTemps pleinTemps = pleinTempsRepository.findById(id).orElse(null);
        if (pleinTemps != null) {
            pleinTemps.setNom(nouveauNom);
            return pleinTempsRepository.save(pleinTemps);
        }
        return null;
    }

    public void afficherPleinTemps(Long id) {
        PleinTemps pleinTemps = pleinTempsRepository.findById(id).orElse(null);
        if (pleinTemps != null) {
            System.out.println("ID: " + pleinTemps.getId() + ", Nom: " + pleinTemps.getNom());
        } else {
            System.out.println("Personne non trouvée pour l'ID: " + id);
        }
    }

    public void supprimerPleinTemps(Long id) {
        pleinTempsRepository.deleteById(id);
    }
}
