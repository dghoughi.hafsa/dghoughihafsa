package org.example.dghoughihafsa.Services;

import org.example.dghoughihafsa.Entity.Adjoint;

import org.example.dghoughihafsa.Repository.AdjointRepository;

import org.springframework.stereotype.Service;

@Service
public class AdjointService{
    private AdjointRepository adjointRepository;

    public AdjointService(AdjointRepository adjointRepository) {
        this.adjointRepository = adjointRepository;
    }
    public Adjoint creerAd(String nom) {
        Adjoint adjoint = new Adjoint();
        adjoint.setNom(nom);
        return adjointRepository.save(adjoint);
    }

    public Adjoint modifierAd(Long id, String nouveauNom) {
        Adjoint adjoint = adjointRepository.findById(id).orElse(null);
        if (adjoint != null) {
            adjoint.setNom(nouveauNom);
            return adjointRepository.save(adjoint);
        }
        return null;
    }

    public void afficherAd(Long id) {
        Adjoint adjoint = adjointRepository.findById(id).orElse(null);
        if (adjoint != null) {
            System.out.println("ID: " + adjoint.getId() + ", Nom: " + adjoint.getNom());
        } else {
            System.out.println("Personne non trouvée pour l'ID: " + id);
        }
    }

    public void supprimerAd(Long id) {
        adjointRepository.deleteById(id);
    }

}
