package org.example.dghoughihafsa.Entity.SuperClasses;

import jakarta.persistence.*;
import org.example.dghoughihafsa.Entity.Universite;
import org.example.dghoughihafsa.Services.UniversiteService;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nom;


    @OneToOne(mappedBy = "person")
    private Universite universite;

    public Person() {
    }

    public Person(String nom) {
        this.nom = nom;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                '}';
    }
}
