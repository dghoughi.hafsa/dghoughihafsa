package org.example.dghoughihafsa.Entity.SuperClasses;

import jakarta.persistence.*;
import org.example.dghoughihafsa.Entity.Departement;
import org.example.dghoughihafsa.Entity.SuperClasses.Person;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Prof extends Person {

    @ManyToOne
    @JoinColumn(
            name = "idDept"
    )
    private Departement departement;

    public Prof() {
    }

    public Prof(String nom) {
        super(nom);
    }
}
