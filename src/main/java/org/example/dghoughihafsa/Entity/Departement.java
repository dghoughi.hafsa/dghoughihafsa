package org.example.dghoughihafsa.Entity;

import jakarta.persistence.*;
import org.example.dghoughihafsa.Entity.SuperClasses.Person;

import java.util.List;

@Entity
public class Departement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idDept;
    private String nomDept;

    @OneToMany
    @JoinColumn(
            name = "idProf"
    )
    private List<Person> profs;

    @ManyToOne
    @JoinColumn(
            name = "idUn"
    )
    private Universite universite3;







    public Departement() {
    }

    public Departement(String nomDept) {
        this.nomDept = nomDept;
    }

    public Long getIdDept() {
        return idDept;
    }

    public void setIdDept(Long idDept) {
        this.idDept = idDept;
    }

    public String getNomDept() {
        return nomDept;
    }

    public void setNomDept(String nomDept) {
        this.nomDept = nomDept;
    }
}
