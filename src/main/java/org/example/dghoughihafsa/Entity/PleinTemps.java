package org.example.dghoughihafsa.Entity;

import jakarta.persistence.Entity;
import org.example.dghoughihafsa.Entity.SuperClasses.Prof;

@Entity
public class PleinTemps extends Prof {
    public PleinTemps() {
    }

    public PleinTemps(String nom) {
        super(nom);
    }
}
