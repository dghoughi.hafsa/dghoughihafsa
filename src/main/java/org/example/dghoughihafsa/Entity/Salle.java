package org.example.dghoughihafsa.Entity;

import jakarta.persistence.*;

@Entity
public class Salle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idSalle;
    private Long nbSiege;

    @ManyToOne
    @JoinColumn(
            name = "universiteid"
    )
    private Universite universite;

    @ManyToOne
    @JoinColumn(
            name = "idUn"
    )
    private Universite universite2;


    public Salle(Long nbSiege, Universite universite) {
        this.nbSiege = nbSiege;
        this.universite = universite;
    }

    public Salle() {
    }

    public Long getIdSalle() {
        return idSalle;
    }

    public void setIdSalle(Long idSalle) {
        this.idSalle = idSalle;
    }

    public Long getNbSiege() {
        return nbSiege;
    }

    public void setNbSiege(Long nbSiege) {
        this.nbSiege = nbSiege;
    }

    public Universite getUniversite() {
        return universite;
    }

    public void setUniversite(Universite universite) {
        this.universite = universite;
    }

    @Override
    public String toString() {
        return "Salle{" +
                "idSalle=" + idSalle +
                ", nbSiege=" + nbSiege +
                ", universite=" + universite +
                '}';
    }
}
