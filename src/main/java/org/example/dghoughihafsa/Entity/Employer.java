package org.example.dghoughihafsa.Entity;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import org.example.dghoughihafsa.Entity.SuperClasses.Person;

@Entity
public class Employer extends Person {
    public Employer() {
    }

    public Employer(String nom) {
        super(nom);
    }

    @ManyToOne
    @JoinColumn(
            name = "idEmp",
            referencedColumnName = "idBureau"
    )
    private Bureau bureau;

}
