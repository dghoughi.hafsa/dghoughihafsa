package org.example.dghoughihafsa.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Bureau {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idBureau;

    public Bureau() {
    }

   @OneToMany(mappedBy = "bureau")
    private List<Employer> employers;

    @ManyToOne
    @JoinColumn(
            name = "idUn"
    )
    private Universite universite;




    public Long getIdBureau() {
        return idBureau;
    }

    public void setIdBureau(Long idBureau) {
        this.idBureau = idBureau;
    }
}
