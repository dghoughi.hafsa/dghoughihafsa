package org.example.dghoughihafsa.Entity;

import jakarta.persistence.Entity;
import org.example.dghoughihafsa.Entity.SuperClasses.Prof;

@Entity
public class Adjoint extends Prof {
    public Adjoint() {
    }

    public Adjoint(String nom) {
        super(nom);
    }
}
